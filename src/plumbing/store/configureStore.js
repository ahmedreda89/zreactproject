import { compose } from 'redux';
import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import { rootEpic, rootReducer } from './root';

const epicMiddleware = createEpicMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

function configureStore() {
	const store = createStore(
		rootReducer,
		composeEnhancers(
			applyMiddleware(epicMiddleware)
		)
	);

	epicMiddleware.run(rootEpic);

	return store;
}

const store = configureStore()
export default store