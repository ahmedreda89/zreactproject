import { combineEpics } from 'redux-observable';
import { combineReducers } from 'redux';
import { catchError } from 'rxjs/operators';

import productsEpic from '../../Products/productsEpic';
import productsReducer from '../../Products/productsReducer';

export const rootEpic = (action$, store$, dependencies) => combineEpics(
		...productsEpic,
)(action$, store$, dependencies).pipe(
	catchError((error, source) => {
		console.error(error);
		return source;
	})
);

export const rootReducer = combineReducers({
	 Products: productsReducer,
});