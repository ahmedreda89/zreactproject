
export function renameFunc(func, name) {
	Object.defineProperty(func, 'name', { writable: true })

	func.name = name

	Object.defineProperty(func, 'name', { writable: false })

	func.displayName = name

	return func
}