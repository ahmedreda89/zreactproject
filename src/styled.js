import styled from 'styled-components'

export const LandingWrapper = styled.div`
	text-align: center;
	padding-top: 20px;
`

export const ContentWrapper = styled.div`
  background-color: #eaeaea;
  padding: 20px;
  height: calc(100vh - 100px);
  overflow: auto;
`

export const AppHeader = styled.div`
  height: 60px;
  color: #fff;
  float: left;
  width: 100%;
  position: relative;
	background-color: #282c34;
  
  h1 {
  	color: #fff;
  	padding: 10px;
  	font-size: 30px;
  	display: inline-block;
  	position: absolute;
  	left: 0;
  }
  
  a {
  	color: #fff;
  }
`

export const Nav = styled.nav`
	text-align: center;
	display: inline-block;
	width: 100%;
	
	a {
  	color: #fff;
  	margin: 20px 10px;
  	display: inline-block;
  }
`

export const TileCard = styled.div`
	width: 70%;
	margin: 15px auto;
	padding: 15px;
	
	background: #ddd;
	border: 1px solid #d2d2d2;
	
	ul {
		width: 50%;
		margin: auto;
		text-align: left;
	}
	
	${({warning}) => {
		return warning
			? `
					color: #856404;
					border-color: #ffeeba;
					background-color: #fff3cd;
				`
			: null
	}}
`