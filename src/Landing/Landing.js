import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp } from '@fortawesome/free-solid-svg-icons'

import { LandingWrapper, TileCard } from '../styled'

export default function Landing(props) {
	return(
		<LandingWrapper>
			<h2>Welcome to the ReactJS task</h2>

			<p>You can start by pressing the products link up there <FontAwesomeIcon icon={faArrowUp} /></p>

			<TileCard>
				This repo is meant to demonstrate the beauty and simplicity of react components using hooks.
			</TileCard>

			<TileCard>
				I used styled components to handle the styles across react components
			</TileCard>

			<TileCard>
				I also used Ant UI elements to speed up things and give it a nice look, <a href="https://ant.design/" target="_blank" rel="noopener noreferrer">check them out they are great!</a>
			</TileCard>

			<TileCard>
				It wasn't necessary to apply redux observable, however I wanted to demonstrate how can we handle extendable stores as our application goes bigger.
			</TileCard>

			<TileCard warning>
				It's a lot of fun, and I already feel sorry for not finding enough time slots to finish this up as fast as it should be.
			</TileCard>

			<TileCard>
				<h3>Todo:</h3>

				<ul>
					<li>Cover all of the core functions with unit tests</li>
					<li>Try out Cypress or Puppeteer</li>
					<li>Build the backend true APIs using ExpressJS(NodeJS)</li>
				</ul>
			</TileCard>

			<TileCard warning>
				<h3>Stay tuned</h3>
				I will be adding features to this repo every couple of days, so you might notice some enhancements and unit tests being added.
				I will also build a NodeJs repo to provide this repo with APIs instead of calling mocki.io APIs.
			</TileCard>
		</LandingWrapper>
	)
}