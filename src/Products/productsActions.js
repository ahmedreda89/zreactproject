import { camelCase as _camelCase, get as _get } from 'lodash'

import { renameFunc } from '../plumbing/utilities'

export const actionTypes = {
	FETCH_ALL_PRODUCTS: 'products/fetch 🚀 💬',
	FETCH_ALL_PRODUCTS_COMPLETED: 'products/fetch 🚀 ✅',
	FETCH_ALL_PRODUCTS_FAILED: 'products/fetch 🚀 ❌',
	FLUSH_PRODUCTS: 'products/flush [flush] 🚽',
}


export const actions = Object.keys(actionTypes)
	.reduce((accum, id) => {
		const creatorName = _camelCase(id)

		const creatorFunction = function _(payload, meta) {
			return {
				type: _get(actionTypes, id),
				payload,
				meta,
			}
		}

		/* eslint-disable no-param-reassign */
		accum[creatorName] = renameFunc(creatorFunction, creatorName)
		/* eslint-enable no-param-reassign */

		return accum
	}, {})
