import { ofType } from 'redux-observable';
import { of, from } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';

import fetchAllProducts from './APIs/fetchAllProducts'
import fetchDepartmentsLookups from './APIs/fetchDepartmentsLookups'
import fetchPromotionsLookups from './APIs/fetchPromotionsLookups'

import {actions, actionTypes} from './productsActions'
import {
	actions as lookupsActions,
	actionTypes as lookupsActionTypes,
} from './Filters/lookupsActions'


const fetchProducts = action$ => action$.pipe(
	ofType(actionTypes.FETCH_ALL_PRODUCTS),
	mergeMap(action =>
		from(fetchAllProducts(action.payload))
		.pipe(
			map(response => {
				// debugger
				return actions.fetchAllProductsCompleted(response)
			}),
			catchError(error => of(actions.fetchAllProductsCompleted(error)))
		)
	)
);


const fetchDepartments = action$ => action$.pipe(
	ofType(lookupsActionTypes.FETCH_ALL_DEPARTMENTS),
	mergeMap(action =>
		from(fetchDepartmentsLookups(action.payload))
		.pipe(
			map(response => {
				// debugger
				return lookupsActions.fetchAllDepartmentsCompleted(response)
			}),
			catchError(error => of(lookupsActions.fetchAllDepartmentsFailed(error)))
		)
	)
);


const fetchPromotions = action$ => action$.pipe(
	ofType(lookupsActionTypes.FETCH_ALL_PROMOTIONS),
	mergeMap(action =>
		from(fetchPromotionsLookups(action.payload))
		.pipe(
			map(response => {
				// debugger
				return lookupsActions.fetchAllPromotionsCompleted(response)
			}),
			catchError(error => of(lookupsActions.fetchAllPromotionsFailed(error)))
		)
	)
);


export default [
	fetchProducts,
	fetchDepartments,
	fetchPromotions,
]