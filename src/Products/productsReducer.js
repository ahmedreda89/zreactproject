import { actionTypes } from './productsActions'
import { actionTypes as lookupsActionTypes } from './Filters/lookupsActions'

const defaultState = {
	list: [],
	meta: {
		isFailed: null,
		isLoading: null,
		isCompleted: null,
	},

	lookups: {
		departments: {
			list: [],
			meta: {
				isFailed: null,
				isLoading: null,
				isCompleted: null,
			},
		},

		promotions: {
			list: [],
			meta: {
				isFailed: null,
				isLoading: null,
				isCompleted: null,
			},
		},
	},
}

export default function productsReducer(state = defaultState, action) {
	// debugger
	switch (action?.type) {
		case actionTypes.FETCH_ALL_PRODUCTS:
			return fetchAllProducts(state, action)

		case actionTypes.FETCH_ALL_PRODUCTS_FAILED:
			return fetchAllProductsFailed(state, action)

		case actionTypes.FETCH_ALL_PRODUCTS_COMPLETED:
			return fetchAllProductsCompleted(state, action)


		case lookupsActionTypes.FETCH_ALL_DEPARTMENTS:
			return fetchAllDepartments(state, action)

		case lookupsActionTypes.FETCH_ALL_DEPARTMENTS_FAILED:
			return fetchAllDepartmentsFailed(state, action)

		case lookupsActionTypes.FETCH_ALL_DEPARTMENTS_COMPLETED:
			return fetchAllDepartmentsCompleted(state, action)


		case lookupsActionTypes.FETCH_ALL_PROMOTIONS:
			return fetchAllPromotions(state, action)

		case lookupsActionTypes.FETCH_ALL_PROMOTIONS_FAILED:
			return fetchAllPromotionsFailed(state, action)

		case lookupsActionTypes.FETCH_ALL_PROMOTIONS_COMPLETED:
			return fetchAllPromotionsCompleted(state, action)

		default:
			return state
	}
}


function fetchAllProducts(store, action) {
	console.log('[productsReducer] fetchAllProducts::action', action)

	return {
		...store,
		list: [],
		meta: {
			isFailed: false,
			isLoading: true,
			isCompleted: false,
		},
	}
}

function fetchAllProductsCompleted(store, action) {
	console.log('[productsReducer] fetchAllProductsCompleted::action', action)

	return {
		...store,
		list: action.payload,
		meta: {
			isFailed: false,
			isLoading: false,
			isCompleted: true,
		},
	}
}

function fetchAllProductsFailed(store, action) {
	console.log('[productsReducer] fetchAllProductsFailed::action', action)

	return {
		...store,
		list: [],
		meta: {
			isFailed: true,
			isLoading: false,
			isCompleted: false,
			error: action.payload.error,
		},
	}
}


function fetchAllDepartments(store, action) {
	console.log('[productsReducer] fetchAllDepartments::action', action)

	return {
		...store,
		lookups: {
			...store.lookups,
			departments: {
				meta: {
					isFailed: false,
					isLoading: true,
					isCompleted: false,
				},
			},
		},
	}
}

function fetchAllDepartmentsCompleted(store, action) {
	console.log('[productsReducer] fetchAllDepartmentsCompleted::action', action)

	return {
		...store,
		lookups: {
			...store.lookups,
			departments: {
				list: action.payload,
				meta: {
					isFailed: false,
					isLoading: false,
					isCompleted: true,
				},
			},
		},
	}
}

function fetchAllDepartmentsFailed(store, action) {
	console.log('[productsReducer] fetchAllDepartmentsFailed::action', action)

	return {
		...store,
		lookups: {
			...store.lookups,
			departments: {
				list: [],
				meta: {
					isFailed: true,
					isLoading: false,
					isCompleted: false,
					error: action.payload.error,
				},
			},
		},
	}
}


function fetchAllPromotions(store, action) {
	console.log('[productsReducer] fetchAllPromotions::action', action)

	return {
		...store,
		lookups: {
			...store.lookups,
			promotions: {
				meta: {
					isFailed: false,
					isLoading: true,
					isCompleted: false,
				},
			},
		},
	}
}

function fetchAllPromotionsCompleted(store, action) {
	console.log('[productsReducer] fetchAllPromotionsCompleted::action', action)

	return {
		...store,
		lookups: {
			...store.lookups,
			promotions: {
				list: action.payload,
				meta: {
					isFailed: false,
					isLoading: false,
					isCompleted: true,
				},
			},
		},
	}
}

function fetchAllPromotionsFailed(store, action) {
	console.log('[productsReducer] fetchAllPromotionsFailed::action', action)

	return {
		...store,
		lookups: {
			...store.lookups,
			promotions: {
				list: [],
				meta: {
					isFailed: true,
					isLoading: false,
					isCompleted: false,
					error: action.payload.error,
				},
			},
		},
	}
}

