import React, { useEffect } from 'react'
import { Table } from 'antd'
import { connect } from 'react-redux'

import Filters from '../Filters'
import { actions } from '../productsActions'

function ProductsList(props) {
	// the empty dependency array means that it will only run once
	useEffect(() => {
		props.fetchAllProducts()
	}, []) // eslint-disable-line

	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Department',
			dataIndex: 'departmentName',
			key: 'department',
		},
		{
			title: 'Promotion',
			dataIndex: 'promotion',
			key: 'promotion',
			render: (promotion, { promotionStatus }) => {
				const isActive = promotionStatus === 'active'
				return isActive
					?	<span>{promotion || '---'}</span>
					: '---'
			},
		},
		{
			title: 'Price',
			dataIndex: 'price',
			key: 'price',
			render: (price, { discountedPrice, promotionStatus }) => {
				return discountedPrice && promotionStatus === 'active'
					?	(
						<span><strike>{price}</strike> {discountedPrice}</span>
					)
					: <span>{price}</span>
			},
		},
	]

	const dispatchFunctions = [
		props.fetchAllProducts,
	]

	return (
		<div>
			<h2>All Products</h2>
			<Filters dispatchFunctions={dispatchFunctions} />
			<Table
				rowKey="name"
				columns={columns}
				loading={props.isLoading}
				dataSource={props.productsList}
			/>
		</div>
	)
}


function mapStateToProps(state) {
	return {
		productsList: state.Products?.list || [],

		isFailed: state.Products?.meta?.isFailed,
		isLoading: state.Products?.meta?.isLoading,
		isCompleted: state.Products?.meta?.isCompleted,

		error: state.Products?.meta?.error,
	}
}

function mapDispatchToProps(dispatch, ownProps) {
	return {
		fetchAllProducts: (...args) => {
			console.log('@@[ProductsList]', 'fetchAllProducts', args)
			dispatch(actions.fetchAllProducts(...args))

			// eslint-disable-next-line no-unused-expressions
			ownProps?.fetchAllProducts?.(...args)
		},
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList)
