import styled from 'styled-components'

export const FilterField = styled.div`
	margin-bottom: 15px;

	label, .ant-select {
		width: 100%;
	}
`