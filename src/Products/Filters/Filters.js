import React, { useState } from 'react';
import { Drawer, Button, message } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilter } from '@fortawesome/free-solid-svg-icons'

import FilterByDepartment from './FilterByDepartment'
import FilterByPromotion from './FilterByPromotion'

export default function Filters(props) {
	const [visible, setVisible] = useState(false);
	const [department, setDepartment] = useState(null);
	const [promotion, setPromotion] = useState(null);

	const showFilterInfoMessage = () => {
		message.info('we are calling fake API that is designed to reply with 200 in all cases, so I commented the query line in the fetchAllProducts API as it returns an error while the status is 200 and crashes tha app, this won\'t be happening if we are calling a real API, however the full redux cycle and filters actions, store manipulations in reducers and epics is done to demonstrate the power of the observable design pattern (using redux observable)');
	};

	const showDrawer = () => {
		setVisible(true);
	};
	const onClose = () => {
		setVisible(false);
	};

	const onClear = () => {
		setDepartment(null)
		setPromotion(null)
		onClose()
	};

	const departmentsChange = (value) => {
		console.log('[Products/Filters] departments::onChange::value ', value)
		setDepartment(value)
	}
	const promotionsChange = (value) => {
		console.log('[Products/Filters] promotions::onChange::value ', value)
		setPromotion(value)
	}

	const submitFilters = () => {
		props.dispatchFunctions.map((fn) => fn(({ department, promotion })))
		onClose()
		showFilterInfoMessage()
		return false
	}

	const renderFiltersDrawer =() => {
		return (
			<Drawer
				closable
				width="25%"
				title="Filters"
				placement="right"
				onClose={onClose}
				visible={visible}
				footer={
					<div
						style={{
							textAlign: 'right',
						}}
					>

						<Button onClick={onClear} style={{ marginRight: 8 }}>
							Clear
						</Button>

						<Button disabled={!department && !promotion} onClick={submitFilters} type="primary">
							Submit
						</Button>
					</div>
				}
			>
				<FilterByDepartment value={department} onChange={departmentsChange} />

				<FilterByPromotion value={promotion} onChange={promotionsChange} />
			</Drawer>
		)
	}

	const filterBtnStyles = { float: 'right', marginBottom: '10px' }

	return (
		<>
			<Button type="primary" style={filterBtnStyles} onClick={showDrawer}>
				<FontAwesomeIcon icon={faFilter} />
			</Button>

			{renderFiltersDrawer()}
		</>
	)
}