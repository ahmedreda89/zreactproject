import { camelCase as _camelCase, get as _get } from 'lodash'

import { renameFunc } from '../../plumbing/utilities'

export const actionTypes = {
	FETCH_ALL_DEPARTMENTS: 'lookups/departments/fetch 🚀 💬',
	FETCH_ALL_DEPARTMENTS_COMPLETED: 'lookups/departments/fetch 🚀 ✅',
	FETCH_ALL_DEPARTMENTS_FAILED: 'lookups/departments/fetch 🚀 ❌',

	FETCH_ALL_PROMOTIONS: 'lookups/promotions/fetch 🚀 💬',
	FETCH_ALL_PROMOTIONS_COMPLETED: 'lookups/promotions/fetch 🚀 ✅',
	FETCH_ALL_PROMOTIONS_FAILED: 'lookups/promotions/fetch 🚀 ❌',
}


export const actions = Object.keys(actionTypes)
	.reduce((accum, id) => {
		const creatorName = _camelCase(id)

		const creatorFunction = function _(payload, meta) {
			return {
				type: _get(actionTypes, id),
				payload,
				meta,
			}
		}

		/* eslint-disable no-param-reassign */
		accum[creatorName] = renameFunc(creatorFunction, creatorName)
		/* eslint-enable no-param-reassign */

		return accum
	}, {})
