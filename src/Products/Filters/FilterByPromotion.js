import React, { useEffect } from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'

import { FilterField } from './styled'
import {actions} from './lookupsActions'

function FilterByPromotions(props) {
	// the empty dependency array means that it will only run once
	useEffect(() => {
		props.fetchPromotionsLookups()
	}, []) // eslint-disable-line

	const selectOptions = (props.promotionsList || [])
		.map(({ promotionStatus, promotion }) => (
			<Select.Option
				key={promotion}
				value={promotion}
				disabled={promotionStatus !== 'active'}
			>
				{promotion}
			</Select.Option>
		))

	return (
		<FilterField>
			<label>Promotions</label>

			<Select
				value={props.value}
				onChange={props.onChange}
				placeholder="Select a promotion"
			>
				{selectOptions}
			</Select>
		</FilterField>
	)
}

function mapStateToProps(state) {
	return {
		promotionsList: state.Products?.lookups?.promotions?.list || [],

		isLoading: state.Products?.lookups?.promotions?.meta?.isLoading,
		isFailed: state.Products?.lookups?.promotions?.meta?.isFailed,
		isCompleted: state.Products?.lookups?.promotions?.meta?.isCompleted,
		error: state.Products?.lookups?.promotions?.meta?.error,
	}
}

function mapDispatchToProps(dispatch, ownProps) {
	return {
		fetchPromotionsLookups: (...args) => {
			console.log('@@[FilterByPromotions]', 'fetchPromotionsLookups', args)
			dispatch(actions.fetchAllPromotions(...args))

			// eslint-disable-next-line no-unused-expressions
			ownProps?.fetchPromotionsLookups?.(...args)
		},
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterByPromotions)