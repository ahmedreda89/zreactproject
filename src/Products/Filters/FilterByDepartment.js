import React, { useEffect } from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'

import { FilterField } from './styled'
import {actions} from './lookupsActions'

function FilterByDepartment(props) {
	// the empty dependency array means that it will only run once
	useEffect(() => {
		props.fetchDepartmentsLookups()
	}, []) // eslint-disable-line

	const selectOptions = (props.departmentsList || [])
		.map(({ departmentId, departmentName }) => (
			<Select.Option
				key={departmentId}
				value={departmentId}
			>
				{departmentName}
			</Select.Option>
		))

	return (
		<FilterField>
			<label>Departments</label>

			<Select
				value={props.value}
				onChange={props.onChange}
				placeholder="Select a department"
			>
				{selectOptions}
			</Select>
		</FilterField>
	)
}

function mapStateToProps(state) {
	return {
		departmentsList: state.Products?.lookups?.departments?.list || [],

		isLoading: state.Products?.lookups?.departments?.meta?.isLoading,
		isFailed: state.Products?.lookups?.departments?.meta?.isFailed,
		isCompleted: state.Products?.lookups?.departments?.meta?.isCompleted,
		error: state.Products?.lookups?.departments?.meta?.error,
	}
}

function mapDispatchToProps(dispatch, ownProps) {
	return {
		fetchDepartmentsLookups: (...args) => {
			console.log('@@[FilterByDepartment]', 'fetchDepartmentsLookups', args)
			dispatch(actions.fetchAllDepartments(...args))

			// eslint-disable-next-line no-unused-expressions
			ownProps?.fetchDepartmentsLookups?.(...args)
		},
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterByDepartment)