import request from 'superagent'


export default function fetchPromotionsLookups(query) {
	return new Promise((resolve, reject) => {
		request
			.get(`https://api.mocki.io/v1/43bd7f75`)
			.send(query || {})
			.then((res) => {
				// debugger // eslint-disable-line
				resolve(res.body)
			})
			.catch((err) => {
				reject(err)
			})
	})
}
