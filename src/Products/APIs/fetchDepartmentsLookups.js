import request from 'superagent'


export default function fetchDepartmentsLookups(query) {
	return new Promise((resolve, reject) => {
		request
			.get(`https://api.mocki.io/v1/554666e8`)
			.send(query || {})
			.then((res) => {
				// debugger // eslint-disable-line
				resolve(res.body)
			})
			.catch((err) => {
				reject(err)
			})
	})
}
