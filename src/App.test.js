import React from 'react';
import App from './App';
import { create } from 'react-test-renderer'

describe('App snapshot test',()=>{
  test('testing app first render', () => {
    let tree = create(<App />)
    expect(tree.toJSON()).toMatchSnapshot();
  })
})