import React from 'react';
import {
  Link,
  Route,
  Switch,
  BrowserRouter as Router,
} from 'react-router-dom'
import 'antd/dist/antd.css'

import Landing from './Landing/Landing';
import ProductsList from './Products/ProductsList';
import { AppHeader, ContentWrapper, Nav } from './styled';


function App() {
  return (
    <Router>
      <div className="App">
        <AppHeader className="App-header">
          <h1>
            <Link to="/">
              <span>ReactJS</span>
            </Link>
          </h1>

          <Nav>
            <Link to="/products">Products</Link>
          </Nav>
        </AppHeader>
      </div>
      <ContentWrapper>
        <Switch>
          <Route
            exact
            path="/"
            component={Landing}
          />

          <Route
            path="/products"
            component={ProductsList}
          />
        </Switch>
      </ContentWrapper>
    </Router>
  );
}

export default App;
